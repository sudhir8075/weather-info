package com.sudhir.weatherinfo.viewmodel

import java.util.*

data class WeatherDataSummaryItem(val temp: Double, val dateCreated: Date)